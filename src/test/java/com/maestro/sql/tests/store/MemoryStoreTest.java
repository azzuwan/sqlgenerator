/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.tests.store;

import com.maestro.sql.entities.EntityType;
import com.maestro.sql.entities.Student;
import com.maestro.sql.store.MemoryStore;
import org.junit.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author azzuwan
 */
public class MemoryStoreTest {

    public MemoryStoreTest() {
    }

    @Test
    public void TestSaveFind() {
        MemoryStore store = new MemoryStore();
        Student s1 = new Student(100, "azzuwan", "male");
        store.save(100, EntityType.STUDENT, s1);
        Student s2 = store.find(100, EntityType.STUDENT);

        Assert.assertTrue(
                s1.getID() == s2.getID()
                && s1.getName() == s2.getName()
                && s1.getGender() == s2.getGender()
        );
    }

    

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
