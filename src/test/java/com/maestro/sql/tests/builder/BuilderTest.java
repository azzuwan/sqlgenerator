/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.tests.builder;

import com.lyncode.jtwig.exception.CompileException;
import com.lyncode.jtwig.exception.ParseException;
import com.lyncode.jtwig.exception.RenderException;
import com.maestro.sql.Bootstrapper;
import com.maestro.sql.builder.Builder;
import com.maestro.sql.entities.EntityType;
import com.maestro.sql.entities.Grade;
import com.maestro.sql.entities.School;
import com.maestro.sql.entities.Student;
import com.maestro.sql.fields.Field;
import com.maestro.sql.joins.Join;
import com.maestro.sql.manager.EntityManager;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author azzuwan
 */
public class BuilderTest {

    public BuilderTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void TestGenerateSingle() {

        try {
            Student student = new Student(100, "azzuwan", "male");
            Grade grade = new Grade(100, "Grade 1");
            //StudentGrade studentGrade = new StudentGrade(100, 100, 100);

            //Using Sql Builder
            Builder builder = new Builder();

            //Creating fields independently. We can also derive fields from a fully
            //instantiated entity. More examples below.
            Field<Student, Integer> studentID = new Field<Student, Integer>(student, "ID", 100);
            Field<Student, String> studentName = new Field<Student, String>(student, "name", "azzuwan");
            Field<Grade, Integer> gradeID = new Field<Grade, Integer>(grade, "ID", 300);

            //Create a join with with student and  grade
            Join<Student, Grade> exampleJoin = new Join<Student, Grade>();
            exampleJoin.setLeft(student);
            exampleJoin.setRight(grade);

            String sql = builder.addField(studentID)
                    .addField(studentName)
                    .addField(gradeID)
                    .addJoin(exampleJoin)
                    .sql();

            //Stupid test because HashSet doesn't guarantee order of elements 
            //that eventually will be output into the template
            //Will device a better scheme later
            String expected = "SELECT student.id AS studentID grade.id AS gradeID student.name FROM student  JOIN Grade ON student.ID = grade.studentID";
            String[] chunks = expected.split(" ");
            boolean match = true;
            for (String s : chunks) {
                if (!sql.contains(s)) {
                    
                    match = false;
                }
            }
            Assert.assertTrue(match);
            

        } catch (ParseException ex) {
            Logger.getLogger(BuilderTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RenderException ex) {
            Logger.getLogger(BuilderTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CompileException ex) {
            Logger.getLogger(BuilderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void TestGenerateList() {
        try {

            Bootstrapper.setup();

            Builder builder = new Builder();
            EntityManager manager = EntityManager.getInstance();
            School school = new School(201, "Rainbow Harbor");
            Field<School, Integer> schoolID = school.getIDField();
            Field<School, String> schoolName = school.getNameField();

            //Bind students to school even without loading a join table.
            //This time load the students from the MemoryStore
            Student studentA = manager.find(EntityType.STUDENT, 120);
            Student studentB = manager.find(EntityType.STUDENT, 130);
            Student studentC = manager.find(EntityType.STUDENT, 135);

            Join<School, Student> join1 = new Join<School, Student>(school, studentA);
            Join<School, Student> join2 = new Join<School, Student>(school, studentB);
            Join<School, Student> join3 = new Join<School, Student>(school, studentC);

            //List of fields received from user columns selection
            //Using hash set so that no duplicate fields will be
            //included in the query generation.
            Set<Field> fields = new HashSet();
            fields.add(schoolID);
            fields.add(schoolName);
            fields.add(studentA.getIDField());
            fields.add(studentA.getNameField());

            Set<Join> joins = new HashSet<Join>();
            joins.add(join1);
            joins.add(join2);
            joins.add(join3);

            //Clear current cache and add Many fields and many joins
            //We can even mix single and multiple additions of fields and joins
            //at any time
            String sql = builder.generate(fields, joins);
            String expected = "SELECT school.name school.id AS schoolID student.name student.id AS studentID FROM school  JOIN Student ON school.ID = student.schoolID Student ON school.ID = student.schoolID Student ON school.ID = student.schoolID";

            
            String[] chunks = expected.split(" ");
            boolean match = true;
            for (String s : chunks) {
                if (!sql.contains(s)) {
                    
                    match = false;
                }
            }
            Assert.assertTrue(match);
            

        } catch (ParseException ex) {
            Logger.getLogger(BuilderTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RenderException ex) {
            Logger.getLogger(BuilderTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CompileException ex) {
            Logger.getLogger(BuilderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
