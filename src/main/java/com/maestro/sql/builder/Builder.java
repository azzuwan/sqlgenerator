/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.builder;

import com.lyncode.jtwig.JtwigContext;
import com.lyncode.jtwig.JtwigModelMap;
import com.lyncode.jtwig.JtwigTemplate;
import com.lyncode.jtwig.exception.CompileException;
import com.lyncode.jtwig.exception.ParseException;
import com.lyncode.jtwig.exception.RenderException;
import com.maestro.sql.exceptions.IllegalFieldListArgumentException;
import com.maestro.sql.exceptions.IllegalJoinListArgumentException;
import com.maestro.sql.fields.Field;
import com.maestro.sql.joins.AbstractJoin;
import com.maestro.sql.joins.Join;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author azzuwan
 */
public class Builder {

    private Set<? super Field> fields = new HashSet();
    private Set<? super AbstractJoin> joins = new HashSet();
    private Set<String> tables = new HashSet<String>();
    private JtwigTemplate template;
    private JtwigModelMap map = new JtwigModelMap();
    private JtwigContext context;
    private String twig
            = "SELECT "
            + "{% for field in fields %}"
            + "{{field.getEntityType()|lower}}."
            + "{{field.fieldName|lower}}"
            + "{% if field.fieldName.toLowerCase() == 'id' %} "
            + "AS {{field.getEntityType()|lower}}{{field.fieldName|upper}}"
            + "{% endif %}"
            + "{% if loop.last == false %}, {% endif %}"
            + "{% endfor %} "
            + "FROM "
            + "{% if joins.isEmpty() == false %}"
            + "{{ joins.iterator().next().getLeft().getEntityType()|lower}} "
            + "{% else %}"
            + "{{ tables.iterator().next()}} "
            + "{% endif %}"            
            + "{% if joins.isEmpty() == false %} "
            + "JOIN "
            + "{% for join in joins %}"
            + "{{join.getRight().getEntityType()}} "
            + "ON {{ join.getLeft().getEntityType()|lower }}.ID = {{ join.getRight().getEntityType()|lower }}.{{ join.getLeft().getEntityType()|lower }}ID "
            + "{% endfor %}"
            + "{% else %}"
            + "{% set left = tables.iterator().next() %}"
            + "JOIN "            
            + "{%for table in tables %}"
            + "{% if table.equals(left) == false %}"
            + "{{table}} ON {{left}}.ID = {{table}}.{{left|lower}}ID "
            + "{% endif %}"            
            + "{% endfor %}"            
            + "{% endif %}";

    public Builder() {

        template = new JtwigTemplate(twig);
        context = new JtwigContext(map);
    }

    public <T extends Field> Builder addField(T field) {
        fields.add(field);
        fields.iterator().next();
        String table = field.getEntityType();
        if (!tables.contains(table)) {
            tables.add(table);
        }
        return this;
    }

    public <T extends Field> Builder addFields(Set<T> fields) {
        this.fields.addAll(fields);
        for (T field : fields) {
            String table = field.getEntityType();
            if (!tables.contains(table)) {
                tables.add(table);
            }
        }
        return this;
    }

    public <T extends Join> Builder addJoin(T join) {
        joins.add(join);
        return this;
    }

    public <T extends Join> Builder addJoins(Set<T> joins) {
        this.joins.addAll(joins);
        return this;
    }

    public String sql() throws ParseException, RenderException, CompileException {        
        map.add("fields", fields);
        map.add("tables", tables);
        map.add("joins", joins);
        String query = template.output(context);
        System.out.println("\n\nGENERATED QUERY: " + query);
        return query;
    }

    public Builder clear() {

        this.template = new JtwigTemplate(twig);
        this.map = new JtwigModelMap();
        this.context = new JtwigContext(map);
        this.fields = new HashSet();
        this.tables = new HashSet<String>();
        this.joins = new HashSet<AbstractJoin>();
        return this;
    }

    public <T extends Field> String generate(Set<T> fields, Set<Join> joins)
            throws ParseException, RenderException, CompileException {
        validateArguments(fields, joins);
        return this.addFields(fields).addJoins(joins).sql();
    }
    
    
    public <T extends Field> String generate(Set<T> fields)
            throws ParseException, RenderException, CompileException {
        validateArguments(fields);
        return this.addFields(fields).sql();
    }
    
    
    private <T extends Field> void validateArguments(Set<T> fields, Set<Join> joins){
        if(fields.isEmpty()){
            throw new IllegalFieldListArgumentException();
        }
        
        if(joins.isEmpty()){
            throw new IllegalJoinListArgumentException();
        }
    }
    
    private <T extends Field> void validateArguments(Set<T> fields){
        if(fields.isEmpty()){
            throw new IllegalFieldListArgumentException();
        }
    }

}
