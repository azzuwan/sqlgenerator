/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql;

import com.lyncode.jtwig.exception.CompileException;
import com.lyncode.jtwig.exception.ParseException;
import com.lyncode.jtwig.exception.RenderException;
import com.maestro.sql.builder.Builder;
import com.maestro.sql.entities.EntityType;
import com.maestro.sql.entities.Grade;
import com.maestro.sql.entities.School;
import com.maestro.sql.entities.Student;
import com.maestro.sql.entities.StudentGrade;
import com.maestro.sql.fields.Field;
import com.maestro.sql.joins.Join;
import com.maestro.sql.manager.EntityManager;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author azzuwan
 */
public class Tester {

    public static void main(String[] args) {

        //Run bootstrapper to setup data;
        Bootstrapper.setup();

        //EntityManager Singleton (EntityLookup)
        //I feel that EntityManager is a better term than EntityLookup
        //because this singleton perform  more than just lookup function
        EntityManager manager = EntityManager.getInstance();

        //Check that singleton works:
        //getStore should return the size of data added in Bootstrapper.setup()
        System.out.println("\n\nSTORE SIZE: "+manager.getStore().size());
        
        System.out.println("\n\nLIST LOOKUP\n");
        
        //Perform Lookup: Find all student
        List<Student> students = manager.findAll(EntityType.STUDENT);
        for (Student student : students) {
            System.out.println(student.getName());
        }
        
        System.out.println("\n");
        
        
        //Lookup all grades
        List<Grade> grades = manager.findAll(EntityType.GRADE);
        for (Grade grade : grades) {
            System.out.println("Grade: " + grade.getName());
        }
        
        System.out.println("\n");
        

        //Lookup all StudentGrade joins        
        List<StudentGrade> studentGrades = manager.findAll(EntityType.STUDENT_GRADE);

        for (StudentGrade studentGrade : studentGrades) {
            System.out.println("Join ID: " + studentGrade.getID() +  " Grade ID: " + studentGrade.getGradeID() + " Student ID: " + studentGrade.getStudentID());
        }

        System.out.println("\n\nSINGLE LOOKUP");

        //Lookup single student
        Student student = manager.find(EntityType.STUDENT, 100);
        System.out.println("Student Name: " + student.getName());

        //Lookup single grade
        Grade grade = manager.find(EntityType.GRADE, 12);
        System.out.println("GRADE NAME: " + grade.getName());

        //Lookup single join
        StudentGrade studentGrade = manager.find(EntityType.STUDENT_GRADE, 50);
        System.out.println("Student Grade: " + studentGrade.getID());
        
        
        //Because we are just working to generate a query, the actual instances
        //of records are not that important for majority of simple use cases. 
        //This demonstration features schema inference from data instances. 
        //The more cost efficient way of course is to just get table metadata
        //or schema representations. However filtering, aggregation and 
        //projection will require more than just schema definitions. That is 
        //where this will be useful.
        
        //Using Sql Builder
        Builder builder = new Builder(); 
        
        //Creating fields independently. We can also derive fields from a fully
        //instantiated entity. More examples below.
        Field<Student,Integer> studentID = new Field<Student,Integer> (student, "ID", 100);
        Field<Student,String> studentName = new Field<Student,String> (student, "name", "azzuwan");
        Field<Grade,Integer> gradeID = new Field<Grade,Integer> (grade, "ID", 300);
        
        //Create a join with with student and  grade 
        Join<Student,Grade> exampleJoin = new Join<Student, Grade>();
        exampleJoin.setLeft(student);
        exampleJoin.setRight(grade);
       
        
        try {
             //Fluent Interface allow us to programmatically add fields 
             // and joins at various phases of the program flow 
             // E.g, keep adding after for loops, filters, http requests, 
             //conditional processing etc. Great for generating highly dynamic queries
            builder.addField(studentID)
                    .addField(studentName)
                    .addField(gradeID)
                    .addJoin(exampleJoin)
                    .sql();
            
            
            //The slightly longer way of binding entity to fields
            School school1 = new School();            
            //Bind fields to school
            Field<School, Integer> schoolID = new Field<School, Integer>(school1,"ID", 101);
            Field<School, String> schoolName = new Field<School, String>(school1,"name", "Treebroke High");
            
            //Bind school to fields
            school1.setIDField(schoolID);
            school1.setNameField(schoolName);
            
            
            //The simpler version
            School school2 = new School(201, "Rainbow Harbor");
            Field<School, Integer> school2ID = school2.getIDField();
            Field<School, String> school2Name = school2.getNameField();
            
            
            //Bind students to school even without loading a join table.
            //This time load the students from the MemoryStore
            Student studentA = manager.find(EntityType.STUDENT, 120);
            Student studentB = manager.find(EntityType.STUDENT, 130);
            Student studentC = manager.find(EntityType.STUDENT, 135);
            
            Join<School,Student> join1 = new Join<School, Student>(school1, studentA);
            Join<School,Student> join2 = new Join<School, Student>(school1, studentB);
            Join<School,Student> join3 = new Join<School, Student>(school1, studentC);
            
            
            //List of fields received from user columns selection
            //Using hash set so that no duplicate fields will be
            //included in the query generation.             
            Set<Field> fields = new HashSet();
            fields.add(schoolID);
            fields.add(schoolName);
            fields.add(studentA.getIDField());
            fields.add(studentA.getNameField());
            
            Set<Join> joins = new HashSet<Join>();
            joins.add(join1);
            joins.add(join2);
            joins.add(join3);
            
            
            //Clear current cache and add Many fields and many joins
            //We can even mix single and multiple additions of fields and joins
            //at any time
            builder.clear()
                    .addFields(fields);
            
            for(int i =0 ; i< 10; i++){
                System.out.println("Processing some conditions that will change the state of the joins or fields...");
            }
            
            //Pickup where we left
            builder.addJoins(joins).sql();
            
            //Using a convenient method
            //This will straight away print the same query
            builder.generate(fields, joins);
            
            //Actually we don't even need the joins. The fields themselves are
            //sufficient enough for us to infer the intent            
            //Applicable only in a purposely crafted UI where only relatable 
            //entities are presented as selection choices
            System.out.println("\n\nJUST THE FIELDS");               
            builder.generate(fields);
            
            //Uncoment to try the exceptions
            //builder.generate(new HashSet<Field>(), new HashSet<Join>());
            //builder.generate(new hashSet<Filed>());
            
        } catch (ParseException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RenderException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CompileException ex) {
            Logger.getLogger(Tester.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        

        

    }

}
