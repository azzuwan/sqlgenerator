/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.entities;

import com.maestro.sql.fields.Field;

/**
 *
 * @author azzuwan
 */
public class Student extends AbstractEntity {

    private Field<Student, Integer> ID;
    private Field<Student, String> name;
    private Field<Student, String> gender;

    public Student() {
    }

    public Student(int ID, String name, String gender) {
        this.ID = new Field<Student, Integer>(this, "ID", ID);
        this.name = new Field<Student, String>(this, "name", name);
        this.gender = new Field<Student, String>(this, "gender", gender);

    }

    /**
     * @return the ID
     */
    public int getID() {
        return ID.getValue().intValue();
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID.setValue(ID);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name.getValue();
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name.setValue(name);
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender.getValue();
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender.setValue(gender);
    }
    
    
    /**
     * @return the IDField Field Object
     */
    public Field<Student, Integer> getIDField() {
        return ID;
    }

    /**
     * @param IDField the ID Field Object to set
     */
    public void setIDField(Field<Student, Integer> IDField) {
        this.ID = IDField;
    }

    /**
     * @return the name field object
     */
    public Field<Student, String> getNameField() {
        return name;
    }

    /**
     * @param nameField the name field to set
     */
    public void setNameField(Field<Student, String> nameField) {
        this.name = nameField;
    }

    /**
     * @return the genderField field object
     */
    public Field<Student,String> getGenderField() {
        return gender;
    }

    /**
     * @param genderField the gender field object to set
     */
    public void setGenderField(Field<Student, String> gender) {
        this.gender =gender;
    }

}
