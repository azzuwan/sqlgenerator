/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.entities;

import com.maestro.sql.fields.Field;

/**
 *
 * @author azzuwan
 */
public class StudentGrade extends AbstractEntity {
    private Field<StudentGrade,Integer> ID;
    private Field<StudentGrade,Integer> studentID;
    private Field<StudentGrade,Integer> gradeID;

    public StudentGrade() {
    }

    public StudentGrade(int ID, int studentID, int gradeID) {
        this.ID = new Field<StudentGrade,Integer>(this,"ID", ID);
        this.studentID= new Field<StudentGrade,Integer>(this, "studentID", studentID);
        this.gradeID = new Field<StudentGrade,Integer>(this, "gradeID", gradeID);
    }

    /**
     * @return the ID
     */
    public int getID() {
        return ID.getValue().intValue();
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID.setValue(ID);
    }

    /**
     * @return the studentID
     */
    public int getStudentID() {
        return studentID.getValue().intValue();
    }

    /**
     * @param studentID the studentID to set
     */
    public void setStudentID(int studentID) {
        this.studentID.setValue(studentID);
    }

    /**
     * @return the gradeID
     */
    public int getGradeID() {
        return gradeID.getValue().intValue();
    }

    /**
     * @param gradeID the gradeID to set
     */
    public void setGradeID(int gradeID) {
        this.gradeID.setValue(gradeID);
    }
    
    
   
}
