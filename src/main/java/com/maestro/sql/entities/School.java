/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.entities;

import com.maestro.sql.fields.Field;

/**
 *
 * @author azzuwan
 */
public class School extends AbstractEntity{
    private Field<School, Integer> ID;
    private Field<School, String> name;    

    public School() {
        this.ID = new Field<School, Integer>();
        this.ID.setEntity(this);
        this.ID.setFieldName("ID");
        this.name = new Field<School, String>();
        this.name.setEntity(this);
        this.name.setFieldName("name");
    }

    public School(Field<School, Integer> ID, Field<School, String> name) {
        this.ID = ID;
        this.name = name;
    }
    
    public School(int ID, String name) {
        this.ID = new Field<School, Integer>(this, "ID", ID);
        this.name = new Field<School, String>(this, "name", name);
    }
    
    

    /**
     * @return the ID
     */
    public int getID() {
        return ID.getValue().intValue();
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID.setValue(ID);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name.getValue();
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name.setValue(name);
    }
    
    
    /**
     * @return the ID field object
     */
    public Field<School, Integer> getIDField() {
        return ID;
    }

    /**
     * @param field the ID to set
     */
    public void setIDField(Field<School, Integer> field) {
        this.ID = field;
    }

    /**
     * @return the name field object
     */
    public Field<School, String> getNameField() {
        return name;
    }

    /**
     * @param field the name field to set
     */
    public void setNameField(Field<School, String> field) {
        this.name = field;
    }
    
}
