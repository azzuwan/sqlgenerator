/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.entities;

import com.maestro.sql.fields.Field;

/**
 *
 * @author azzuwan
 */
public class Grade extends AbstractEntity {
    private Field<Grade, Integer> ID;
    private Field<Grade, String> name;

    public Grade() {
    }

    public Grade(int ID, String name) {
        this.ID = new Field<Grade,Integer> (this, "ID", ID);
        this.name = new Field<Grade,String> (this, "name", name);
              
    }

    /**
     * @return the ID
     */
    public int getID() {
        return ID.getValue().intValue();
    }

    /**
     * @param ID the ID to set
     */
    public void setID(int ID) {
        this.ID.setValue(ID);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name.getValue();
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name.setValue(name);
    }
    
    /**
     * @return the ID field object
     */
    public Field<Grade, Integer> getIDField() {
        return ID;
    }

    /**
     * @param ID  the ID field object set
     */
    public void setIDField(Field<Grade, Integer> ID) {
        this.ID = ID;
    }

    /**
     * @return the name field object
     */
    public Field<Grade, String> getNameField() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setNameField(Field<Grade, String> field) {
        this.name = field;
    }
    
    
    
    

}
