/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.manager;

import com.maestro.sql.entities.AbstractEntity;
import com.maestro.sql.entities.EntityType;
import com.maestro.sql.store.Store;
import java.util.List;

/**
 *
 * @author azzuwan
 */
//Non-DI, Non-HashMap version using manual Synchronized Double Checked Locking Singleton
//
public class EntityManager{
    
    private  Store store;
    
    private EntityManager(){}
    
    private static volatile EntityManager instance;
    
    public static EntityManager getInstance(){
        if( instance == null){
            synchronized(EntityManager.class){
                if(instance == null){
                    instance = new EntityManager();
                }
            }
        }        
        return instance;
    }
    
    public <T extends AbstractEntity> T find(EntityType type, int ID){
        return store.find(ID, type);
    }
    
    public <T extends AbstractEntity> List<T> findAll(EntityType type){
        return store.findAll(type);
    }
    
    public <T extends AbstractEntity> T create(EntityType type, int ID, T entity){
        return store.save(ID, type, entity);
    }
    
    public <T extends AbstractEntity> T remove(EntityType type, int ID){
        return store.remove(ID, type);
    }
    
    public <T extends AbstractEntity> T update(EntityType type, int ID, T entity){
        return store.save(ID, type, entity);
    }
    
    

    /**
     * @return the store
     */
    public Store getStore() {
        return store;
    }

    /**
     * @param store the store to set
     */
    public void setStore(Store store) {
        this.store = store;
    }
    
}