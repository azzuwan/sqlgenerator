/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.exceptions;

/**
 *
 * @author azzuwan
 */
public class IllegalFieldListArgumentException extends IllegalArgumentException{

    public IllegalFieldListArgumentException() {
        super("You are trying to pass an empty field list");
    }

    public IllegalFieldListArgumentException(String s) {
        super(s);
    }

    public IllegalFieldListArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalFieldListArgumentException(Throwable cause) {
        super(cause);
    }
    
    
    
}
