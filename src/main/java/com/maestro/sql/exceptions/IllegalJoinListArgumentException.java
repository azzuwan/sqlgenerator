/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.exceptions;

/**
 *
 * @author azzuwan
 */
public class IllegalJoinListArgumentException extends IllegalArgumentException{

    public IllegalJoinListArgumentException() {
        super("You are trying to pass an empty join list");
    }

    public IllegalJoinListArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalJoinListArgumentException(Throwable cause) {
        super(cause);
    }
    
}
