/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.fields;

import com.maestro.sql.entities.AbstractEntity;

/**
 *
 * @author azzuwan
 */
public class Field<E extends AbstractEntity,V> extends AbstractField{    
    
    private E entity;
    private V value;
    private String fieldName;

    public Field() {
    }
    
    

    public Field(E entity, String fieldName, V value) {
        this.entity = entity;
        this.value = value;
        this.fieldName = fieldName;
    }
    
    

    /**
     * @return the entity
     */
    public E getEntity() {
        return entity;
    }

    /**
     * @param entity the entity to set
     */
    public void setEntity(E entity) {
        this.entity = entity;
    }

    /**
     * @return the value
     */
    public V getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(V value) {
        this.value = value;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }
    
    public String getEntityType(){
        return entity.getClass().getSimpleName();
    }
    
}
