/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.maestro.sql.generator;

import com.lyncode.jtwig.exception.CompileException;
import com.lyncode.jtwig.exception.ParseException;
import com.lyncode.jtwig.exception.RenderException;
import com.maestro.sql.builder.Builder;
import com.maestro.sql.fields.Field;
import com.maestro.sql.joins.Join;
import java.util.Set;

/**
 *
 * @author azzuwan
 */

public class SqlGenerator{
    private static volatile SqlGenerator instance;
    private SqlGenerator(){}
    
    public static  SqlGenerator  getInstance(){
        if(instance == null){
            synchronized(SqlGenerator.class){
                //Double checked locking
                if(instance == null){
                    instance = new SqlGenerator();            
                }
            }            
        }
        return instance;
    }
    
    public <T extends Field, J extends Join>String generate(Set<T> fields, Set<J> joins) throws ParseException, RenderException, CompileException{
        Builder builder = new Builder();
        return builder
                .addFields(fields)
                .addJoins(joins)
                .sql();
    }

    
}


