/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.store;

/**
 *
 * @author azzuwan
 */
public enum StorageType {
    MEMORY, SQL, REST, EJB, RMI
}
