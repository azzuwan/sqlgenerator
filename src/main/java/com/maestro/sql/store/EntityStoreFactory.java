/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.store;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author azzuwan
 */
//HashMap Based factory to avoid having a lot of switches for different
//Store types
public class EntityStoreFactory {

    private static Map<StorageType, Store> storeList = new HashMap<StorageType, Store>();

    //Eager initialization
    private EntityStoreFactory() {

        storeList.put(StorageType.MEMORY, new MemoryStore());
        storeList.put(StorageType.SQL, new SqlStore());
        storeList.put(StorageType.REST, new RESTStore());
        storeList.put(StorageType.EJB, new EJBStore());
    }

    private static volatile EntityStoreFactory instance;

    public static Store get(StorageType storageType) {
        if (instance == null) {
            instance = new EntityStoreFactory();

            if (storeList == null) {
                synchronized (storeList) {
                    Store store = storeList.get(storageType);
                    return store;
                }
            } else {
                return storeList.get(storageType);
            }
        }else{
            return storeList.get(storageType);
        }

    }
}
