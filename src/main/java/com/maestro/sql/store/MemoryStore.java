/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.store;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.maestro.sql.entities.AbstractEntity;
import com.maestro.sql.entities.EntityType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 *
 * @author azzuwan
 */
public class MemoryStore implements Store{
        
    private static Table<Integer, EntityType, ? super AbstractEntity> entities;
    
    public MemoryStore( ){
        entities = HashBasedTable.create();
    }    


    @Override
    public <T extends AbstractEntity> T find(int ID, EntityType type) {
        return (T) entities.get(ID, type);
    }

    @Override
    public <T extends AbstractEntity> List<T> findAll(EntityType type) {
        Map<Integer, T> records;
        records = (Map<Integer, T>) entities.column(type);                
        return new ArrayList<T>(records.values());
    }

    @Override
    public <T extends AbstractEntity> T save(int ID, EntityType type, T value) {
        entities.put(ID, type, value);
        return  value;
    }

    @Override
    public <T extends AbstractEntity> T remove(int ID, EntityType type) {
        
        return (T) entities.remove(ID, type);
    }
    
    public int size(){
        return entities.size();
    }
    
    
    
    
}
