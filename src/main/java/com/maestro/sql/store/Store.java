/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.store;

import com.maestro.sql.entities.AbstractEntity;
import com.maestro.sql.entities.EntityType;
import java.util.List;

/**
 *
 * @author azzuwan
 * @param <T>
 */
public interface Store{
    public <T extends AbstractEntity> T  find(int ID, EntityType type);
    public <T extends AbstractEntity> List<T> findAll(EntityType type); 
    public <T extends AbstractEntity> T save(int ID, EntityType type, T  value);
    public <T extends AbstractEntity> T remove(int ID, EntityType type);
    public int size();
}
