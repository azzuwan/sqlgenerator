/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql;

import com.maestro.sql.entities.EntityType;
import com.maestro.sql.entities.Grade;
import com.maestro.sql.entities.Student;
import com.maestro.sql.entities.StudentGrade;
import com.maestro.sql.manager.EntityManager;
import com.maestro.sql.store.EntityStoreFactory;
import com.maestro.sql.store.MemoryStore;
import com.maestro.sql.store.StorageType;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author azzuwan
 */
public class Bootstrapper {

    private static final EntityManager manager = EntityManager.getInstance();
    private static final MemoryStore store = (MemoryStore) EntityStoreFactory.get(StorageType.MEMORY);
    private static final List<String> names = new ArrayList<String>();
    private static final List<String> genders = new ArrayList<String>();
    private static final List<String> gradeNames = new ArrayList<String>();
    private static final List<Grade> grades = new ArrayList<Grade>();
    private static final List<StudentGrade> studentGrades = new ArrayList<StudentGrade>();
    private static final Random rand = new Random();

    public static void setup() {
        manager.setStore(store);

        genders.add("male");
        genders.add("female");

        names.add("Jordon Furby");
        names.add("Danelle Weyant");
        names.add("Von Harryman");
        names.add("Margaret Zhu");
        names.add("Junie Bright");
        names.add("Dena Scharff");
        names.add("Adah Hillier");
        names.add("Claribel Hoffmann");
        names.add("Reed Woolverton");
        names.add("Kimbery Mortellaro");
        names.add("Ty Vanhoy");
        names.add("Thelma Degroff");
        names.add("Alishia Willman");
        names.add("Yolonda Zwart");
        names.add("Wallace Corrado");
        names.add("Vickie Ertl");
        names.add("Emile Collinsworth");
        names.add("Jose Fullam");
        names.add("Lesia Sachs");
        names.add("Milda Meagher");
        names.add("Aileen Duffin");
        names.add("Winfred Lipka");
        names.add("Gina Applebee");
        names.add("Kemberly Mccrea");
        names.add("Julius Coonrod");
        names.add("Eva Donofrio");
        names.add("Renate Jessee");
        names.add("Christal Su");
        names.add("Inell Seigler");
        names.add("Adelaide Carlon");
        names.add("Hollis Egli");
        names.add("Era Mutter");
        names.add("Georgeanna Hubler");
        names.add("Rosella Tignor");
        names.add("Reyes Mcandrews");
        names.add("Leonarda Rm");
        names.add("Lashawnda Comerford");
        names.add("Del Bialaszewski");
        names.add("Pilar Hemenway");
        names.add("Leigh Trybus");
        names.add("Kyra Gwozdz");
        names.add("Rebbecca Mckittrick");
        names.add("Denice Smythe");
        names.add("Suk Whitsitt");
        names.add("Wanda Fahey");
        names.add("Jeane Behrman");
        names.add("Lashawn Bagwell");
        names.add("Danyell Euell");
        names.add("Shawnda Humber");
        names.add("Tijuana Noto");

        gradeNames.add("Grade 1");
        gradeNames.add("Grade 2");
        gradeNames.add("Grade 3");
        gradeNames.add("Grade 4");
        gradeNames.add("Grade 5");
        gradeNames.add("Grade 6");
        gradeNames.add("Grade 7");
        gradeNames.add("Grade 8");
        gradeNames.add("Grade 9");
        gradeNames.add("Grade 10");
        gradeNames.add("Grade 11");
        gradeNames.add("Grade 12");

        for (int i = 0; i < names.size(); i++) {
            int id = 100 + i;
            Student student = new Student(id, names.get(i), genders.get(rand.nextInt(1)));
            manager.create(EntityType.STUDENT, id, student);
        }
        
        for(int i = 0; i < gradeNames.size(); i++){
            
            grades.add(new Grade(i+1, gradeNames.get(i)));
            manager.create(EntityType.GRADE, i+1, new Grade(i+1, gradeNames.get(i)));
        }
        

        List<Student> students = manager.findAll(EntityType.STUDENT);
        
        
        int n = 0;
        for(int i =0; i < students.size(); i++){
            
            if( i%5 == 0){
                ++n;
            }
            studentGrades.add(new StudentGrade(i + 1, students.get(i).getID(), grades.get(n).getID()));
            manager.create(EntityType.STUDENT_GRADE, i+1, new StudentGrade(i + 1, students.get(i).getID(), grades.get(n).getID()));
        }
        

    }

}
