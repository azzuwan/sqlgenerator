/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.maestro.sql.joins;

import com.maestro.sql.entities.AbstractEntity;

/**
 *
 * @author azzuwan
 */
public class Join<L extends AbstractEntity, R extends AbstractEntity> extends AbstractJoin  {
    private L left;
    private R right;

    public Join() {
    }

    public Join(L left, R right) {
        this.left = left;
        this.right = right;
    }
    
    
    /**
     * @return the left
     */
    public L getLeft() {
        return left;
    }

    /**
     * @param left the left to set
     */
    public void setLeft(L left) {
        this.left = left;
    }

    /**
     * @return the right
     */
    public R getRight() {
        return right;
    }

    /**
     * @param right the right to set
     */
    public void setRight(R right) {
        this.right = right;
    }
}
